#include <stdlib.h>
#include <limits.h>
#include <starpu.h>
#define TAMANHO_VETOR 27
#define DIVISAO 3
#define NUM_DIVISOES (TAMANHO_VETOR/DIVISAO)

int vetor[TAMANHO_VETOR];

void func_cpu(void *buffers[], void *args)
{
        int *lista = (int *)STARPU_VECTOR_GET_PTR(buffers[0]);
	int *resposta = (int *)STARPU_VECTOR_GET_PTR(buffers[1]);

	int maior = INT_MIN;

	for(int i=0; i<DIVISAO; i++){
		if(maior<lista[i]){
			maior=lista[i];
		}
	}

	resposta[0] = maior;
}

struct starpu_codelet mycodelet =
{
        .cpu_funcs = { func_cpu },
        .cpu_funcs_name = { "func_cpu" },
        .nbuffers = 2,
        .modes = { STARPU_R, STARPU_W },
	.where = STARPU_CPU,
	.name = "MD3"
};

struct starpu_data_filter filter_lista =
{
    .filter_func = starpu_vector_filter_block,
    .nchildren = NUM_DIVISOES
};

struct starpu_data_filter filter_resposta =
{
    .filter_func = starpu_vector_filter_block,
    .nchildren = DIVISAO
};

int ipow(int base, int exp)
{
    int result = 1;
    for (;;)
    {
        if (exp & 1)
            result *= base;
        exp >>= 1;
        if (!exp)
            break;
        base *= base;
    }

    return result;
}

int main(){
	
	//Vamos ler alguns valores
	for(int i=0; i<TAMANHO_VETOR; i++){
		vetor[i] = i;
		//scanf("%d", vetor[i])
	}

	starpu_init(NULL);
	
	/* Declare data to StarPU */
	starpu_data_handle_t handle_lista;
	starpu_vector_data_register(&handle_lista, STARPU_MAIN_RAM, (uintptr_t)vetor, TAMANHO_VETOR, sizeof(vetor[0]));
	/* Partition the vector in PARTS sub-vectors */

	//Particiona os dados
	starpu_data_partition(handle_lista, &filter_lista);
	
	starpu_data_handle_t handles_entrada[NUM_DIVISOES];
	starpu_data_handle_t handles_resposta[NUM_DIVISOES];

	for(int i=0; i<NUM_DIVISOES; i++){
		handles_entrada[i] = starpu_data_get_sub_data(handle_lista, 1, i);
	}
	
	int tamanho_arvore = log(TAMANHO_VETOR)/log(DIVISAO);
	printf("Tamanho: %d\n", tamanho_arvore);

	for(int n=0; n<(tamanho_arvore-1); n++){
		printf("level:%d\n", n);
		for(int i=0; i<ipow(DIVISAO, DIVISAO-1-n); i++){
			int reposta_id = i/DIVISAO;
			printf("reposta_id:%d\n", reposta_id);
			if(i%DIVISAO==0){
				starpu_vector_data_register(&handles_resposta[reposta_id], -1, 0, DIVISAO, sizeof(int));
				starpu_data_partition(handles_resposta[reposta_id], &filter_resposta);
			}

			starpu_data_handle_t sub_resposta = starpu_data_get_sub_data(handles_resposta[reposta_id], 1, i%DIVISAO);
			
			starpu_task_insert(&mycodelet,
                                STARPU_R, handles_entrada[i],
                                STARPU_W, sub_resposta,
                                0);

			if(i%DIVISAO==DIVISAO-1){
				starpu_data_unpartition(handles_resposta[reposta_id], STARPU_MAIN_RAM);
			}
		}
		memcpy(handles_entrada, handles_resposta, sizeof(handles_resposta));
	}
	
	//Ultima tarefa
	int resposta_final[1];
	starpu_vector_data_register(&handles_resposta[0], STARPU_MAIN_RAM, (uintptr_t)resposta_final, 1, sizeof(int));
	starpu_task_insert(&mycodelet,
                           STARPU_R, handles_entrada[0],
			   STARPU_W, handles_resposta[0],
                           0);
	starpu_task_wait_for_all();
	printf("Resultado final:%d\n", resposta_final[0]);

	starpu_shutdown();
	
}
