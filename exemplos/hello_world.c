#include <stdlib.h>
#include <limits.h>
#include <starpu.h>

void func_cpu(void *buffers[], void *args)
{
    printf("Hello World!\n");
}

struct starpu_codelet codelet_world =
{
    .cpu_funcs = { func_cpu },
    .nbuffers = 0,
    .name = "hello_world",
};

int main(){
    starpu_init(NULL);
    starpu_topology_print(stdout);
    starpu_task_insert(&codelet_world, 0);
    starpu_task_wait_for_all();
    starpu_shutdown();
}
